﻿namespace GUI
{
    partial class ContractorsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractorsView));
            this.ribbonControlContractors = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDelete = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageOptions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.layoutControlContractors = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlContractors = new DevExpress.XtraGrid.GridControl();
            this.gridViewContractors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnContractorsType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditContractorsType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnContractorsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsAddressNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsZipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsNip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsRegon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractorsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LayoutControlGroupContractors = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlContractors)).BeginInit();
            this.layoutControlContractors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditContractorsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroupContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControlContractors
            // 
            this.ribbonControlContractors.ExpandCollapseItem.Id = 0;
            this.ribbonControlContractors.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControlContractors.ExpandCollapseItem,
            this.barButtonItemNew,
            this.barButtonItemEdit,
            this.barButtonItemDelete,
            this.barButtonItemRefresh});
            this.ribbonControlContractors.Location = new System.Drawing.Point(0, 0);
            this.ribbonControlContractors.MaxItemId = 5;
            this.ribbonControlContractors.Name = "ribbonControlContractors";
            this.ribbonControlContractors.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageOptions});
            this.ribbonControlContractors.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControlContractors.Size = new System.Drawing.Size(1222, 143);
            this.ribbonControlContractors.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemNew
            // 
            this.barButtonItemNew.Caption = "New";
            this.barButtonItemNew.Id = 1;
            this.barButtonItemNew.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemNew.ImageOptions.Image")));
            this.barButtonItemNew.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemNew.ImageOptions.LargeImage")));
            this.barButtonItemNew.Name = "barButtonItemNew";
            this.barButtonItemNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemNew_ItemClick);
            // 
            // barButtonItemEdit
            // 
            this.barButtonItemEdit.Caption = "Edit";
            this.barButtonItemEdit.Id = 2;
            this.barButtonItemEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemEdit.ImageOptions.Image")));
            this.barButtonItemEdit.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemEdit.ImageOptions.LargeImage")));
            this.barButtonItemEdit.Name = "barButtonItemEdit";
            this.barButtonItemEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEdit_ItemClick);
            // 
            // barButtonItemDelete
            // 
            this.barButtonItemDelete.Caption = "Delete";
            this.barButtonItemDelete.Id = 3;
            this.barButtonItemDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemDelete.ImageOptions.Image")));
            this.barButtonItemDelete.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemDelete.ImageOptions.LargeImage")));
            this.barButtonItemDelete.Name = "barButtonItemDelete";
            this.barButtonItemDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDelete_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "Refresh";
            this.barButtonItemRefresh.Id = 4;
            this.barButtonItemRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.ImageOptions.Image")));
            this.barButtonItemRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.ImageOptions.LargeImage")));
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // ribbonPageOptions
            // 
            this.ribbonPageOptions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupActions});
            this.ribbonPageOptions.Name = "ribbonPageOptions";
            this.ribbonPageOptions.Text = "Options";
            // 
            // ribbonPageGroupActions
            // 
            this.ribbonPageGroupActions.ItemLinks.Add(this.barButtonItemNew);
            this.ribbonPageGroupActions.ItemLinks.Add(this.barButtonItemEdit);
            this.ribbonPageGroupActions.ItemLinks.Add(this.barButtonItemDelete);
            this.ribbonPageGroupActions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupActions.Name = "ribbonPageGroupActions";
            this.ribbonPageGroupActions.Text = "Actions";
            // 
            // layoutControlContractors
            // 
            this.layoutControlContractors.Controls.Add(this.gridControlContractors);
            this.layoutControlContractors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlContractors.Location = new System.Drawing.Point(0, 143);
            this.layoutControlContractors.Name = "layoutControlContractors";
            this.layoutControlContractors.Root = this.LayoutControlGroupContractors;
            this.layoutControlContractors.Size = new System.Drawing.Size(1222, 585);
            this.layoutControlContractors.TabIndex = 1;
            this.layoutControlContractors.Text = "layoutControl1";
            // 
            // gridControlContractors
            // 
            this.gridControlContractors.Location = new System.Drawing.Point(12, 12);
            this.gridControlContractors.MainView = this.gridViewContractors;
            this.gridControlContractors.MenuManager = this.ribbonControlContractors;
            this.gridControlContractors.Name = "gridControlContractors";
            this.gridControlContractors.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditContractorsType});
            this.gridControlContractors.Size = new System.Drawing.Size(1198, 561);
            this.gridControlContractors.TabIndex = 4;
            this.gridControlContractors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewContractors});
            // 
            // gridViewContractors
            // 
            this.gridViewContractors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnContractorsType,
            this.gridColumnContractorsName,
            this.gridColumnContractorsFirstName,
            this.gridColumnContractorsLastName,
            this.gridColumnContractorsEmail,
            this.gridColumnContractorsPhone,
            this.gridColumnContractorsWebsite,
            this.gridColumnContractorsStreet,
            this.gridColumnContractorsAddressNumber,
            this.gridColumnContractorsZipCode,
            this.gridColumnContractorsCity,
            this.gridColumnContractorsCountry,
            this.gridColumnContractorsNip,
            this.gridColumnContractorsRegon,
            this.gridColumnContractorsId});
            this.gridViewContractors.GridControl = this.gridControlContractors;
            this.gridViewContractors.Name = "gridViewContractors";
            this.gridViewContractors.OptionsBehavior.Editable = false;
            this.gridViewContractors.OptionsBehavior.ReadOnly = true;
            this.gridViewContractors.OptionsScrollAnnotations.ShowSelectedRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewContractors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewContractors.OptionsSelection.MultiSelect = true;
            // 
            // gridColumnContractorsType
            // 
            this.gridColumnContractorsType.Caption = "Typ kontrahenta";
            this.gridColumnContractorsType.ColumnEdit = this.repositoryItemLookUpEditContractorsType;
            this.gridColumnContractorsType.FieldName = "ContractorType";
            this.gridColumnContractorsType.Name = "gridColumnContractorsType";
            this.gridColumnContractorsType.Visible = true;
            this.gridColumnContractorsType.VisibleIndex = 0;
            // 
            // repositoryItemLookUpEditContractorsType
            // 
            this.repositoryItemLookUpEditContractorsType.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemLookUpEditContractorsType.AutoHeight = false;
            this.repositoryItemLookUpEditContractorsType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditContractorsType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEditContractorsType.DisplayMember = "Name";
            this.repositoryItemLookUpEditContractorsType.Name = "repositoryItemLookUpEditContractorsType";
            this.repositoryItemLookUpEditContractorsType.NullText = "";
            this.repositoryItemLookUpEditContractorsType.ValueMember = "ContractorType";
            // 
            // gridColumnContractorsName
            // 
            this.gridColumnContractorsName.Caption = "Nazwa";
            this.gridColumnContractorsName.FieldName = "Name";
            this.gridColumnContractorsName.Name = "gridColumnContractorsName";
            this.gridColumnContractorsName.Visible = true;
            this.gridColumnContractorsName.VisibleIndex = 1;
            // 
            // gridColumnContractorsFirstName
            // 
            this.gridColumnContractorsFirstName.Caption = "Imię";
            this.gridColumnContractorsFirstName.FieldName = "FirstName";
            this.gridColumnContractorsFirstName.Name = "gridColumnContractorsFirstName";
            this.gridColumnContractorsFirstName.Visible = true;
            this.gridColumnContractorsFirstName.VisibleIndex = 2;
            // 
            // gridColumnContractorsLastName
            // 
            this.gridColumnContractorsLastName.Caption = "Nazwisko";
            this.gridColumnContractorsLastName.FieldName = "LastName";
            this.gridColumnContractorsLastName.Name = "gridColumnContractorsLastName";
            this.gridColumnContractorsLastName.Visible = true;
            this.gridColumnContractorsLastName.VisibleIndex = 3;
            // 
            // gridColumnContractorsEmail
            // 
            this.gridColumnContractorsEmail.Caption = "E-mail";
            this.gridColumnContractorsEmail.FieldName = "Email";
            this.gridColumnContractorsEmail.Name = "gridColumnContractorsEmail";
            this.gridColumnContractorsEmail.Visible = true;
            this.gridColumnContractorsEmail.VisibleIndex = 4;
            // 
            // gridColumnContractorsPhone
            // 
            this.gridColumnContractorsPhone.Caption = "Numer telefonu";
            this.gridColumnContractorsPhone.FieldName = "Phone";
            this.gridColumnContractorsPhone.Name = "gridColumnContractorsPhone";
            this.gridColumnContractorsPhone.Visible = true;
            this.gridColumnContractorsPhone.VisibleIndex = 5;
            // 
            // gridColumnContractorsWebsite
            // 
            this.gridColumnContractorsWebsite.Caption = "Strona www";
            this.gridColumnContractorsWebsite.FieldName = "Website";
            this.gridColumnContractorsWebsite.Name = "gridColumnContractorsWebsite";
            this.gridColumnContractorsWebsite.Visible = true;
            this.gridColumnContractorsWebsite.VisibleIndex = 6;
            // 
            // gridColumnContractorsStreet
            // 
            this.gridColumnContractorsStreet.Caption = "Ulica";
            this.gridColumnContractorsStreet.FieldName = "Street";
            this.gridColumnContractorsStreet.Name = "gridColumnContractorsStreet";
            this.gridColumnContractorsStreet.Visible = true;
            this.gridColumnContractorsStreet.VisibleIndex = 7;
            // 
            // gridColumnContractorsAddressNumber
            // 
            this.gridColumnContractorsAddressNumber.Caption = "Numer porządkowy";
            this.gridColumnContractorsAddressNumber.FieldName = "AddressNumber";
            this.gridColumnContractorsAddressNumber.Name = "gridColumnContractorsAddressNumber";
            this.gridColumnContractorsAddressNumber.Visible = true;
            this.gridColumnContractorsAddressNumber.VisibleIndex = 8;
            // 
            // gridColumnContractorsZipCode
            // 
            this.gridColumnContractorsZipCode.Caption = "Kod pocztowy";
            this.gridColumnContractorsZipCode.FieldName = "ZipCode";
            this.gridColumnContractorsZipCode.Name = "gridColumnContractorsZipCode";
            this.gridColumnContractorsZipCode.Visible = true;
            this.gridColumnContractorsZipCode.VisibleIndex = 9;
            // 
            // gridColumnContractorsCity
            // 
            this.gridColumnContractorsCity.Caption = "Miejscowość";
            this.gridColumnContractorsCity.FieldName = "City";
            this.gridColumnContractorsCity.Name = "gridColumnContractorsCity";
            this.gridColumnContractorsCity.Visible = true;
            this.gridColumnContractorsCity.VisibleIndex = 10;
            // 
            // gridColumnContractorsCountry
            // 
            this.gridColumnContractorsCountry.Caption = "Kraj";
            this.gridColumnContractorsCountry.FieldName = "Country";
            this.gridColumnContractorsCountry.Name = "gridColumnContractorsCountry";
            this.gridColumnContractorsCountry.Visible = true;
            this.gridColumnContractorsCountry.VisibleIndex = 11;
            // 
            // gridColumnContractorsNip
            // 
            this.gridColumnContractorsNip.Caption = "NIP";
            this.gridColumnContractorsNip.FieldName = "Nip";
            this.gridColumnContractorsNip.Name = "gridColumnContractorsNip";
            this.gridColumnContractorsNip.Visible = true;
            this.gridColumnContractorsNip.VisibleIndex = 12;
            // 
            // gridColumnContractorsRegon
            // 
            this.gridColumnContractorsRegon.Caption = "REGON";
            this.gridColumnContractorsRegon.FieldName = "Regon";
            this.gridColumnContractorsRegon.Name = "gridColumnContractorsRegon";
            this.gridColumnContractorsRegon.Visible = true;
            this.gridColumnContractorsRegon.VisibleIndex = 13;
            // 
            // gridColumnContractorsId
            // 
            this.gridColumnContractorsId.Caption = "Id";
            this.gridColumnContractorsId.FieldName = "Id";
            this.gridColumnContractorsId.Name = "gridColumnContractorsId";
            // 
            // LayoutControlGroupContractors
            // 
            this.LayoutControlGroupContractors.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroupContractors.GroupBordersVisible = false;
            this.LayoutControlGroupContractors.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.LayoutControlGroupContractors.Name = "LayoutControlGroupContractors";
            this.LayoutControlGroupContractors.Size = new System.Drawing.Size(1222, 585);
            this.LayoutControlGroupContractors.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlContractors;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1202, 565);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ContractorsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 728);
            this.Controls.Add(this.layoutControlContractors);
            this.Controls.Add(this.ribbonControlContractors);
            this.Name = "ContractorsView";
            this.Ribbon = this.ribbonControlContractors;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contractors";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlContractors)).EndInit();
            this.layoutControlContractors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditContractorsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroupContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControlContractors;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageOptions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupActions;
        private DevExpress.XtraBars.BarButtonItem barButtonItemNew;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEdit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDelete;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraLayout.LayoutControl layoutControlContractors;
        private DevExpress.XtraGrid.GridControl gridControlContractors;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewContractors;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroupContractors;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsLastName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsPhone;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsWebsite;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsStreet;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsAddressNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsZipCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsCity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsCountry;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsNip;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsRegon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractorsId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditContractorsType;
    }
}

