﻿using DevExpress.XtraEditors;
using Model.Contractors;
using System;
using System.Windows.Forms;

namespace GUI
{
    public partial class EditContractorForm : XtraForm
    {
        private Contractor _originalContractor;
        private Contractor _contractorClone;

        public EditContractorForm(Contractor contractor)
        {
            InitializeComponent();

            _originalContractor = contractor ?? new Contractor();
            _contractorClone = (Contractor)_originalContractor.Clone();

            Text = _contractorClone.IsNew ? "Nowy kontrahent" : "Edycja kontrahenta";

            lookUpEditContractorType.Properties.DataSource = ContractorTypeRepresentation.Types;
            lookUpEditContractorType.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.ContractorType), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditName.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Name), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditFirstName.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.FirstName), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditLastName.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.LastName), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditEmail.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Email), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditPhoneNumber.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Phone), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditWebsite.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Website), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditAddressStreet.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Street), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditAddressNumber.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.AddressNumber), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditAddressZipCode.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.ZipCode), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditAddressCity.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.City), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditAddressCountry.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Country), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditRegon.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Regon), true, DataSourceUpdateMode.OnPropertyChanged);
            textEditNip.DataBindings.Add("EditValue", _contractorClone, nameof(Contractor.Nip), true, DataSourceUpdateMode.OnPropertyChanged);

            dxErrorProvider.DataSource = _contractorClone;
        }

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            if (_contractorClone.IsValid())
            {
                _originalContractor.CopyFrom(_contractorClone);
                DialogResult = DialogResult.OK;
            }

            dxErrorProvider.RefreshControls();
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void lookUpEditContractorType_EditValueChanged(object sender, EventArgs e)
        {
            if (!(sender is LookUpEdit lookUpEdit) ||
                !(lookUpEdit.EditValue is ContractorType contractorType))
                return;

            textEditName.Enabled = contractorType == ContractorType.Company;
            textEditFirstName.Enabled = contractorType == ContractorType.Person;
            textEditLastName.Enabled = contractorType == ContractorType.Person;
            textEditRegon.Enabled = contractorType == ContractorType.Company;

            switch (contractorType)
            {
                case ContractorType.Person:
                    _contractorClone.Name = null;
                    _contractorClone.Regon = null;
                    break;
                case ContractorType.Company:
                    _contractorClone.FirstName = null;
                    _contractorClone.LastName = null;
                    break;
            }
        }
    }
}