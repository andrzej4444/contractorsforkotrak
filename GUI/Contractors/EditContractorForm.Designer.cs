﻿namespace GUI
{
    partial class EditContractorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControlEditContractor = new DevExpress.XtraLayout.LayoutControl();
            this.LayoutControlGroupEditContractor = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.textEditFirstName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemFirstName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lookUpEditContractorType = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItemContractorType = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditRegon = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemRegon = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditNip = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemNip = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditAddressStreet = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemAddressStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textEditAddressNumber = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemAddressNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditAddressZipCode = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemAddressZipCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditAddressCity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemAddressCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditAddressCountry = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemAddressCountry = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupAdditionalData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditPhoneNumber = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemPhoneNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditWebsite = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupContactData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItemOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItemCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEditContractor)).BeginInit();
            this.layoutControlEditContractor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroupEditContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditContractorType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRegon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressStreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressZipCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressZipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAdditionalData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhoneNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupContactData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlEditContractor
            // 
            this.layoutControlEditContractor.Controls.Add(this.simpleButtonCancel);
            this.layoutControlEditContractor.Controls.Add(this.simpleButtonOk);
            this.layoutControlEditContractor.Controls.Add(this.textEditWebsite);
            this.layoutControlEditContractor.Controls.Add(this.textEditPhoneNumber);
            this.layoutControlEditContractor.Controls.Add(this.textEditEmail);
            this.layoutControlEditContractor.Controls.Add(this.textEditAddressCountry);
            this.layoutControlEditContractor.Controls.Add(this.textEditAddressCity);
            this.layoutControlEditContractor.Controls.Add(this.textEditAddressZipCode);
            this.layoutControlEditContractor.Controls.Add(this.textEditAddressNumber);
            this.layoutControlEditContractor.Controls.Add(this.textEditAddressStreet);
            this.layoutControlEditContractor.Controls.Add(this.textEditNip);
            this.layoutControlEditContractor.Controls.Add(this.textEditRegon);
            this.layoutControlEditContractor.Controls.Add(this.textEditLastName);
            this.layoutControlEditContractor.Controls.Add(this.lookUpEditContractorType);
            this.layoutControlEditContractor.Controls.Add(this.textEditFirstName);
            this.layoutControlEditContractor.Controls.Add(this.textEditName);
            this.layoutControlEditContractor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlEditContractor.Location = new System.Drawing.Point(0, 0);
            this.layoutControlEditContractor.Name = "layoutControlEditContractor";
            this.layoutControlEditContractor.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(588, 446, 650, 400);
            this.layoutControlEditContractor.Root = this.LayoutControlGroupEditContractor;
            this.layoutControlEditContractor.Size = new System.Drawing.Size(536, 571);
            this.layoutControlEditContractor.TabIndex = 0;
            this.layoutControlEditContractor.Text = "layoutControl1";
            // 
            // LayoutControlGroupEditContractor
            // 
            this.LayoutControlGroupEditContractor.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroupEditContractor.GroupBordersVisible = false;
            this.LayoutControlGroupEditContractor.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroupMain,
            this.layoutControlGroupAddress,
            this.layoutControlGroupAdditionalData,
            this.layoutControlGroupContactData,
            this.layoutControlItemOk,
            this.layoutControlItemCancel,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.LayoutControlGroupEditContractor.Name = "Root";
            this.LayoutControlGroupEditContractor.Size = new System.Drawing.Size(536, 571);
            this.LayoutControlGroupEditContractor.TextVisible = false;
            // 
            // textEditName
            // 
            this.textEditName.Enabled = false;
            this.textEditName.Location = new System.Drawing.Point(127, 66);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(385, 20);
            this.textEditName.StyleController = this.layoutControlEditContractor;
            this.textEditName.TabIndex = 4;
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemName.Text = "Nazwa:";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 540);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(516, 11);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // textEditFirstName
            // 
            this.textEditFirstName.Enabled = false;
            this.textEditFirstName.Location = new System.Drawing.Point(127, 90);
            this.textEditFirstName.Name = "textEditFirstName";
            this.textEditFirstName.Size = new System.Drawing.Size(385, 20);
            this.textEditFirstName.StyleController = this.layoutControlEditContractor;
            this.textEditFirstName.TabIndex = 5;
            // 
            // layoutControlItemFirstName
            // 
            this.layoutControlItemFirstName.Control = this.textEditFirstName;
            this.layoutControlItemFirstName.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemFirstName.Name = "layoutControlItemFirstName";
            this.layoutControlItemFirstName.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemFirstName.Text = "Imię:";
            this.layoutControlItemFirstName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // lookUpEditContractorType
            // 
            this.lookUpEditContractorType.Location = new System.Drawing.Point(127, 42);
            this.lookUpEditContractorType.Name = "lookUpEditContractorType";
            this.lookUpEditContractorType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEditContractorType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditContractorType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpEditContractorType.Properties.DisplayMember = "Name";
            this.lookUpEditContractorType.Properties.DropDownRows = 2;
            this.lookUpEditContractorType.Properties.NullText = "";
            this.lookUpEditContractorType.Properties.ShowFooter = false;
            this.lookUpEditContractorType.Properties.ShowHeader = false;
            this.lookUpEditContractorType.Properties.ValueMember = "ContractorType";
            this.lookUpEditContractorType.Size = new System.Drawing.Size(385, 20);
            this.lookUpEditContractorType.StyleController = this.layoutControlEditContractor;
            this.lookUpEditContractorType.TabIndex = 6;
            this.lookUpEditContractorType.EditValueChanged += new System.EventHandler(this.lookUpEditContractorType_EditValueChanged);
            // 
            // layoutControlItemContractorType
            // 
            this.layoutControlItemContractorType.Control = this.lookUpEditContractorType;
            this.layoutControlItemContractorType.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemContractorType.Name = "layoutControlItemContractorType";
            this.layoutControlItemContractorType.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemContractorType.Text = "Rodzaj kontrahenta:";
            this.layoutControlItemContractorType.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditLastName
            // 
            this.textEditLastName.Enabled = false;
            this.textEditLastName.Location = new System.Drawing.Point(127, 114);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Size = new System.Drawing.Size(385, 20);
            this.textEditLastName.StyleController = this.layoutControlEditContractor;
            this.textEditLastName.TabIndex = 7;
            // 
            // layoutControlItemLastName
            // 
            this.layoutControlItemLastName.Control = this.textEditLastName;
            this.layoutControlItemLastName.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemLastName.Name = "layoutControlItemLastName";
            this.layoutControlItemLastName.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemLastName.Text = "Nazwisko:";
            this.layoutControlItemLastName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditRegon
            // 
            this.textEditRegon.Enabled = false;
            this.textEditRegon.Location = new System.Drawing.Point(127, 456);
            this.textEditRegon.Name = "textEditRegon";
            this.textEditRegon.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEditRegon.Properties.Mask.EditMask = "\\d\\d\\d\\d\\d\\d\\d\\d\\d";
            this.textEditRegon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEditRegon.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditRegon.Properties.MaxLength = 9;
            this.textEditRegon.Size = new System.Drawing.Size(385, 20);
            this.textEditRegon.StyleController = this.layoutControlEditContractor;
            this.textEditRegon.TabIndex = 8;
            // 
            // layoutControlItemRegon
            // 
            this.layoutControlItemRegon.Control = this.textEditRegon;
            this.layoutControlItemRegon.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemRegon.Name = "layoutControlItemRegon";
            this.layoutControlItemRegon.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemRegon.Text = "REGON:";
            this.layoutControlItemRegon.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditNip
            // 
            this.textEditNip.Location = new System.Drawing.Point(127, 480);
            this.textEditNip.Name = "textEditNip";
            this.textEditNip.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEditNip.Properties.Mask.EditMask = "\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d";
            this.textEditNip.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEditNip.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditNip.Properties.MaxLength = 10;
            this.textEditNip.Size = new System.Drawing.Size(385, 20);
            this.textEditNip.StyleController = this.layoutControlEditContractor;
            this.textEditNip.TabIndex = 9;
            // 
            // layoutControlItemNip
            // 
            this.layoutControlItemNip.Control = this.textEditNip;
            this.layoutControlItemNip.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemNip.Name = "layoutControlItemNip";
            this.layoutControlItemNip.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemNip.Text = "NIP:";
            this.layoutControlItemNip.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditAddressStreet
            // 
            this.textEditAddressStreet.Location = new System.Drawing.Point(127, 294);
            this.textEditAddressStreet.Name = "textEditAddressStreet";
            this.textEditAddressStreet.Size = new System.Drawing.Size(385, 20);
            this.textEditAddressStreet.StyleController = this.layoutControlEditContractor;
            this.textEditAddressStreet.TabIndex = 10;
            // 
            // layoutControlItemAddressStreet
            // 
            this.layoutControlItemAddressStreet.Control = this.textEditAddressStreet;
            this.layoutControlItemAddressStreet.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemAddressStreet.Name = "layoutControlItemAddressStreet";
            this.layoutControlItemAddressStreet.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemAddressStreet.Text = "Ulica:";
            this.layoutControlItemAddressStreet.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroupMain
            // 
            this.layoutControlGroupMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemContractorType,
            this.layoutControlItemName,
            this.layoutControlItemFirstName,
            this.layoutControlItemLastName});
            this.layoutControlGroupMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMain.Name = "layoutControlGroupMain";
            this.layoutControlGroupMain.Size = new System.Drawing.Size(516, 138);
            this.layoutControlGroupMain.Text = "Dane podstawowe";
            // 
            // textEditAddressNumber
            // 
            this.textEditAddressNumber.Location = new System.Drawing.Point(127, 318);
            this.textEditAddressNumber.Name = "textEditAddressNumber";
            this.textEditAddressNumber.Size = new System.Drawing.Size(385, 20);
            this.textEditAddressNumber.StyleController = this.layoutControlEditContractor;
            this.textEditAddressNumber.TabIndex = 11;
            // 
            // layoutControlItemAddressNumber
            // 
            this.layoutControlItemAddressNumber.Control = this.textEditAddressNumber;
            this.layoutControlItemAddressNumber.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemAddressNumber.Name = "layoutControlItemAddressNumber";
            this.layoutControlItemAddressNumber.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemAddressNumber.Text = "Numer porządkowy:";
            this.layoutControlItemAddressNumber.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditAddressZipCode
            // 
            this.textEditAddressZipCode.Location = new System.Drawing.Point(127, 342);
            this.textEditAddressZipCode.Name = "textEditAddressZipCode";
            this.textEditAddressZipCode.Size = new System.Drawing.Size(385, 20);
            this.textEditAddressZipCode.StyleController = this.layoutControlEditContractor;
            this.textEditAddressZipCode.TabIndex = 12;
            // 
            // layoutControlItemAddressZipCode
            // 
            this.layoutControlItemAddressZipCode.Control = this.textEditAddressZipCode;
            this.layoutControlItemAddressZipCode.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemAddressZipCode.Name = "layoutControlItemAddressZipCode";
            this.layoutControlItemAddressZipCode.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemAddressZipCode.Text = "Kod pocztowy:";
            this.layoutControlItemAddressZipCode.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditAddressCity
            // 
            this.textEditAddressCity.Location = new System.Drawing.Point(127, 366);
            this.textEditAddressCity.Name = "textEditAddressCity";
            this.textEditAddressCity.Size = new System.Drawing.Size(385, 20);
            this.textEditAddressCity.StyleController = this.layoutControlEditContractor;
            this.textEditAddressCity.TabIndex = 13;
            // 
            // layoutControlItemAddressCity
            // 
            this.layoutControlItemAddressCity.Control = this.textEditAddressCity;
            this.layoutControlItemAddressCity.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemAddressCity.Name = "layoutControlItemAddressCity";
            this.layoutControlItemAddressCity.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemAddressCity.Text = "Miejscowość:";
            this.layoutControlItemAddressCity.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditAddressCountry
            // 
            this.textEditAddressCountry.Location = new System.Drawing.Point(127, 390);
            this.textEditAddressCountry.Name = "textEditAddressCountry";
            this.textEditAddressCountry.Size = new System.Drawing.Size(385, 20);
            this.textEditAddressCountry.StyleController = this.layoutControlEditContractor;
            this.textEditAddressCountry.TabIndex = 14;
            // 
            // layoutControlItemAddressCountry
            // 
            this.layoutControlItemAddressCountry.Control = this.textEditAddressCountry;
            this.layoutControlItemAddressCountry.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItemAddressCountry.Name = "layoutControlItemAddressCountry";
            this.layoutControlItemAddressCountry.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemAddressCountry.Text = "Kraj:";
            this.layoutControlItemAddressCountry.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroupAddress
            // 
            this.layoutControlGroupAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAddressStreet,
            this.layoutControlItemAddressCountry,
            this.layoutControlItemAddressNumber,
            this.layoutControlItemAddressZipCode,
            this.layoutControlItemAddressCity});
            this.layoutControlGroupAddress.Location = new System.Drawing.Point(0, 252);
            this.layoutControlGroupAddress.Name = "layoutControlGroupAddress";
            this.layoutControlGroupAddress.Size = new System.Drawing.Size(516, 162);
            this.layoutControlGroupAddress.Text = "Adres";
            // 
            // layoutControlGroupAdditionalData
            // 
            this.layoutControlGroupAdditionalData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemRegon,
            this.layoutControlItemNip});
            this.layoutControlGroupAdditionalData.Location = new System.Drawing.Point(0, 414);
            this.layoutControlGroupAdditionalData.Name = "layoutControlGroupAdditionalData";
            this.layoutControlGroupAdditionalData.Size = new System.Drawing.Size(516, 90);
            this.layoutControlGroupAdditionalData.Text = "Dane dodatkowe";
            // 
            // textEditEmail
            // 
            this.textEditEmail.Location = new System.Drawing.Point(127, 180);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Size = new System.Drawing.Size(385, 20);
            this.textEditEmail.StyleController = this.layoutControlEditContractor;
            this.textEditEmail.TabIndex = 15;
            // 
            // layoutControlItemEmail
            // 
            this.layoutControlItemEmail.Control = this.textEditEmail;
            this.layoutControlItemEmail.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemEmail.Name = "layoutControlItemEmail";
            this.layoutControlItemEmail.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemEmail.Text = "E-mail:";
            this.layoutControlItemEmail.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditPhoneNumber
            // 
            this.textEditPhoneNumber.Location = new System.Drawing.Point(127, 204);
            this.textEditPhoneNumber.Name = "textEditPhoneNumber";
            this.textEditPhoneNumber.Size = new System.Drawing.Size(385, 20);
            this.textEditPhoneNumber.StyleController = this.layoutControlEditContractor;
            this.textEditPhoneNumber.TabIndex = 16;
            // 
            // layoutControlItemPhoneNumber
            // 
            this.layoutControlItemPhoneNumber.Control = this.textEditPhoneNumber;
            this.layoutControlItemPhoneNumber.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemPhoneNumber.Name = "layoutControlItemPhoneNumber";
            this.layoutControlItemPhoneNumber.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemPhoneNumber.Text = "Numer telefonu:";
            this.layoutControlItemPhoneNumber.TextSize = new System.Drawing.Size(99, 13);
            // 
            // textEditWebsite
            // 
            this.textEditWebsite.Location = new System.Drawing.Point(127, 228);
            this.textEditWebsite.Name = "textEditWebsite";
            this.textEditWebsite.Size = new System.Drawing.Size(385, 20);
            this.textEditWebsite.StyleController = this.layoutControlEditContractor;
            this.textEditWebsite.TabIndex = 17;
            // 
            // layoutControlItemWebsite
            // 
            this.layoutControlItemWebsite.Control = this.textEditWebsite;
            this.layoutControlItemWebsite.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemWebsite.Name = "layoutControlItemWebsite";
            this.layoutControlItemWebsite.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItemWebsite.Text = "Strona www:";
            this.layoutControlItemWebsite.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroupContactData
            // 
            this.layoutControlGroupContactData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemEmail,
            this.layoutControlItemPhoneNumber,
            this.layoutControlItemWebsite});
            this.layoutControlGroupContactData.Location = new System.Drawing.Point(0, 138);
            this.layoutControlGroupContactData.Name = "layoutControlGroupContactData";
            this.layoutControlGroupContactData.Size = new System.Drawing.Size(516, 114);
            this.layoutControlGroupContactData.Text = "Dane kontaktowe";
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.Location = new System.Drawing.Point(270, 526);
            this.simpleButtonOk.MaximumSize = new System.Drawing.Size(125, 0);
            this.simpleButtonOk.MinimumSize = new System.Drawing.Size(125, 0);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(125, 22);
            this.simpleButtonOk.StyleController = this.layoutControlEditContractor;
            this.simpleButtonOk.TabIndex = 18;
            this.simpleButtonOk.Text = "OK";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonOk_Click);
            // 
            // layoutControlItemOk
            // 
            this.layoutControlItemOk.Control = this.simpleButtonOk;
            this.layoutControlItemOk.Location = new System.Drawing.Point(258, 514);
            this.layoutControlItemOk.Name = "layoutControlItemOk";
            this.layoutControlItemOk.Size = new System.Drawing.Size(129, 26);
            this.layoutControlItemOk.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOk.TextVisible = false;
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(399, 526);
            this.simpleButtonCancel.MaximumSize = new System.Drawing.Size(125, 0);
            this.simpleButtonCancel.MinimumSize = new System.Drawing.Size(125, 0);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(125, 22);
            this.simpleButtonCancel.StyleController = this.layoutControlEditContractor;
            this.simpleButtonCancel.TabIndex = 19;
            this.simpleButtonCancel.Text = "Anuluj";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // layoutControlItemCancel
            // 
            this.layoutControlItemCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemCancel.Location = new System.Drawing.Point(387, 514);
            this.layoutControlItemCancel.Name = "layoutControlItemCancel";
            this.layoutControlItemCancel.Size = new System.Drawing.Size(129, 26);
            this.layoutControlItemCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 504);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(516, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 514);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(258, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // EditContractorForm
            // 
            this.AcceptButton = this.simpleButtonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(536, 571);
            this.Controls.Add(this.layoutControlEditContractor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "EditContractorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit contractor";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEditContractor)).EndInit();
            this.layoutControlEditContractor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroupEditContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditContractorType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRegon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressStreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressZipCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressZipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAddressCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAddressCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAdditionalData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhoneNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupContactData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlEditContractor;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private DevExpress.XtraEditors.TextEdit textEditWebsite;
        private DevExpress.XtraEditors.TextEdit textEditPhoneNumber;
        private DevExpress.XtraEditors.TextEdit textEditEmail;
        private DevExpress.XtraEditors.TextEdit textEditAddressCountry;
        private DevExpress.XtraEditors.TextEdit textEditAddressCity;
        private DevExpress.XtraEditors.TextEdit textEditAddressZipCode;
        private DevExpress.XtraEditors.TextEdit textEditAddressNumber;
        private DevExpress.XtraEditors.TextEdit textEditAddressStreet;
        private DevExpress.XtraEditors.TextEdit textEditNip;
        private DevExpress.XtraEditors.TextEdit textEditRegon;
        private DevExpress.XtraEditors.TextEdit textEditLastName;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditContractorType;
        private DevExpress.XtraEditors.TextEdit textEditFirstName;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroupEditContractor;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemContractorType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFirstName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLastName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddressStreet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddressCountry;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddressNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddressZipCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAddressCity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAdditionalData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRegon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNip;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupContactData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhoneNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWebsite;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOk;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCancel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
    }
}