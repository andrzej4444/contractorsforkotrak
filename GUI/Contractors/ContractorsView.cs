﻿using Dal.Contractors;
using DevExpress.XtraBars;
using Model.Contractors;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GUI
{
    public partial class ContractorsView : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ContractorsDbContext ContractorsDbContext { get; set; } = new ContractorsDbContext();

        public ContractorsView()
        {
            InitializeComponent();
            repositoryItemLookUpEditContractorsType.DataSource = ContractorTypeRepresentation.Types;
            SetDataSource();
        }

        private void barButtonItemNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            var contractor = new Contractor();

            if (new EditContractorForm(contractor).ShowDialog() == DialogResult.OK)
            {
                ContractorsDbContext.Contractors.Add(contractor);
                ContractorsDbContext.SaveChanges();
                gridViewContractors.RefreshData();
                SetDataSource();
                SelectContractor(contractor);
            }
        }

        private void barButtonItemEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!(gridViewContractors.GetFocusedRow() is Contractor contractor))
            {
                MessageBox.Show(
                    "Nie zaznaczono kontrahenta do edycji!",
                    "Edycja kontrahenta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            if (new EditContractorForm(contractor).ShowDialog() == DialogResult.OK)
            {
                ContractorsDbContext.SaveChanges();
                gridViewContractors.RefreshData();
            }
        }

        private void barButtonItemDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gridViewContractors.SelectedRowsCount == 0)
            {
                MessageBox.Show(
                    "Nie zaznaczono żadnych kontrahentów!",
                    "Usuwanie kontrahentów",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            if (MessageBox.Show(
                "Czy na pewno chcesz usunąć zaznaczonych kontrahentów?",
                "Usuwanie kontrahentów",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question)
                == DialogResult.No)
                return;

            var contractorsToRemove = new List<Contractor>();

            foreach (int rowHandle in gridViewContractors.GetSelectedRows())
            {
                if (gridViewContractors.GetRow(rowHandle) is Contractor contractor)
                {
                    contractorsToRemove.Add(contractor);
                }
            }

            foreach (var contractor in contractorsToRemove)
            {
                ContractorsDbContext.Contractors.Remove(contractor);
            }

            ContractorsDbContext.SaveChanges();
            SetDataSource();
        }

        private void barButtonItemRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetDataSource();
        }

        private void SelectContractor(Contractor contractor)
        {
            var index = (gridControlContractors.DataSource as List<Contractor>)?.FindIndex(c => c.Id == contractor.Id);

            if (index !=null && index < 0)
                return;

            int rowHandle = gridViewContractors.GetRowHandle((int)index);
            gridViewContractors.ClearSelection();
            gridViewContractors.SelectRow(rowHandle);
            gridViewContractors.FocusedRowHandle = rowHandle;
        }

        private void SetDataSource()
        {
            gridControlContractors.DataSource = ContractorsDbContext.Contractors.ToList();
        }
    }
}
