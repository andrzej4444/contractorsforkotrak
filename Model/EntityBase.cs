﻿using DevExpress.XtraEditors.DXErrorProvider;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class EntityBase : IDXDataErrorInfo, ICloneable
    {
        public int Id { get; set; }
        public bool IsNew => Id == 0;
        private Dictionary<string, string> Errors { get; set; } = new Dictionary<string, string>();

        #region IDXDataErrorInfo

        public void GetError(ErrorInfo info)
        {
            if (Errors.Any())
            {
                info.ErrorType = ErrorType.Critical;
            }
        }

        public void GetPropertyError(string propertyName, ErrorInfo info)
        {
            if (Errors.ContainsKey(propertyName))
            {
                info.ErrorText = Errors[propertyName];
            }
        }

        #endregion

        public bool IsValid()
        {
            Errors.Clear();
            Validate();
            return !Errors.Any();
        }

        protected virtual void Validate() {}

        protected void AddError(string propertyName, string error)
        {
            if (!Errors.ContainsKey(propertyName))
            {
                Errors.Add(propertyName, error);
            }
        }

        public void CopyFrom<T>(T source)
        {
            if (typeof(T) != GetType())
                return;

            foreach (var property in source.GetType().GetProperties().Where(p => p.SetMethod != null))
            {
                GetType().GetProperty(property.Name)?.SetValue(this, property.GetValue(source));
            }
        }

        #region ICloneable

        public object Clone()
        {
            var newObject = Activator.CreateInstance(GetType());

            foreach (var property in GetType().GetProperties().Where(p => p.SetMethod != null))
            {
                newObject.GetType().GetProperty(property.Name)?.SetValue(newObject, property.GetValue(this));
            }

            return newObject;
        }

        #endregion
    }
}
