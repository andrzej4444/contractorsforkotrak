﻿namespace Model.Contractors
{
    public enum ContractorType
    {
        None,
        Person,
        Company
    }
}
