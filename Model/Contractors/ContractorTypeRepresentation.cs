﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Model.Contractors
{
    public class ContractorTypeRepresentation
    {
        public static ReadOnlyCollection<ContractorTypeRepresentation> Types { get; } = new ReadOnlyCollection<ContractorTypeRepresentation>(
            new List<ContractorTypeRepresentation>
            {
                new ContractorTypeRepresentation(ContractorType.Person, "Osoba fizyczna"),
                new ContractorTypeRepresentation(ContractorType.Company, "Firma")
            });

        public ContractorType ContractorType { get; }
        public string Name { get; }

        private ContractorTypeRepresentation(ContractorType contractorType, string name)
        {
            ContractorType = contractorType;
            Name = name;
        }
    }
}
