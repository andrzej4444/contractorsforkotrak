﻿using System.ComponentModel.DataAnnotations;

namespace Model.Contractors
{
    public class Contractor : EntityBase
    {
        [Required]
        public ContractorType ContractorType { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string Street { get; set; }
        public string AddressNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Nip { get; set; }
        public string Regon { get; set; }

        protected override void Validate()
        {
            base.Validate();

            if (ContractorType == ContractorType.None)
            {
                AddError(nameof(ContractorType), "Nie wybrano rodzaju kontrahenta!");
            }
            else if (ContractorType == ContractorType.Person)
            {
                if (string.IsNullOrWhiteSpace(FirstName))
                {
                    AddError(nameof(FirstName), "Nie wypełniono imienia!");
                }

                if (string.IsNullOrWhiteSpace(LastName))
                {
                    AddError(nameof(LastName), "Nie wypełniono nazwiska!");
                }
            }
            else if (ContractorType == ContractorType.Company)
            {
                if (string.IsNullOrWhiteSpace(Name))
                {
                    AddError(nameof(Name), "Nie wypełniono nazwy firmy!");
                }
            }

            if (!string.IsNullOrWhiteSpace(Regon) && Regon.Length != 9)
            {
                AddError(nameof(Regon), "Niepoprawny format REGON!");
            }

            if (!string.IsNullOrWhiteSpace(Nip) && Nip.Length != 10)
            {
                AddError(nameof(Nip), "Niepoprawny format NIP!");
            }
        }
    }
}
