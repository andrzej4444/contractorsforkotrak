﻿using Model.Contractors;
using System.Data.Entity;

namespace Dal.Contractors
{
    public class ContractorsDbContext : DbContext
    {
        public ContractorsDbContext() : base("ContractorsByAndrzejHenek")
        {
        }

        public DbSet<Contractor> Contractors { get; set; }
    }
}
